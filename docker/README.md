# Build all containers

$ ./build.sh


# Setup remote debugging for Open Liberty web server

- Look in the file jvm.options for instructions
- Make sure you open up for port 7777 in docker-compose.yml. e.g:

            ports:
                - "7777:7777"

- If you have more containers with debugging turned on, you of course have to open different ports for each container

             ports:
                 - "7777:7777"
             [...]
             ports:
                 - "7778:7777"    

- Start the container and Connect with debug from your IDE

# Tips för Windows

Docker vill gärna ha unix radbrytningar i Dockerfile o ev andra bash-skript som hamnar i containarna. När man hämtar från git så omvandlas de (än så länge) till windows-radbrytningar så det resulterarar i att containarna inte fungerar.
För att bygga docker, se till att build.sh skripten samt Dockerfile mm har unix-radbrytningar.
Kör skriptet convert2unix.sh efter när man gjort git pull i tex. 'Git Bash'

Brandväggen kan störa så att docker inte hittar de filer som hamnar i dropins/ mapparna. Lägg in en exception eller stäng av brandväggen.
Det kan även vara så att man behöver starta om 'Shared drives' i 'Settings' i Docker for windows, dvs klicka ur -> klicka spara, klicka i -> klicka spara

# merges from hsa/docker
build.old.sh was build.sh in hsa/docker
hsa-openliberty-servlet-old ~= hsa-openliberty-ldap
hsa-openliberty-microprofile1-old ~= hsa-openliberty-rest
hsa-redis-data-old ~= hsa-redis-data
hsa-redis-old ~= hsa-redis