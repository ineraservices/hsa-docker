# HSA SonarQube

This is a docker-compose suite which sets up SonarQube with a PostgreSQL database.

It is running on a VM at NMT, and the address is https://hsa-sonarqube.nordicmedtest.se

Credentials is being kept in the dhsa project at OpenShift.

hsa-s2i-backend is set up to call Sonarqube on build
