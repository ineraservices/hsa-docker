#### HSA Jenkins

This is the Jenkins image that runs all our pipelines.

It is based on the OpenShift adopted CentOS-jenkins image that RedHat supplies, but with the addition of some plugins.

When adding or modifying plugins to the list, make sure that the plugins will load with the Jenkins version that comes with this image. The Jenkins version can't be upgraded, since that can cause OpenShift-related plugins to no longer work. The easiest way of making sure it works is to build and run the image locally, and make sure there are no plugin load errors in the startup log.

For more info on installing Jenkins in OpenShift. See hsa.git/openshift/README.md.
