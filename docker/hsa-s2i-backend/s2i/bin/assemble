#!/bin/bash -e
#
# S2I assemble script for the 'whatever' image.
# The 'assemble' script builds your application source so that it is ready to run.
#
# For more information refer to the documentation:
#   https://github.com/openshift/source-to-image/blob/master/docs/builder_image.md
#

# place .gradle stuff in /tmp
export GRADLE_USER_HOME=/tmp/.gradle

S2I_SOURCE_DIR=${S2I_SOURCE_DIR-"/tmp/src"}

# Resulting WAR files will be copied to this location
S2I_ARTIFACTS_DIR=${S2I_ARTIFACTS_DIR-"/tmp/artifacts"}

# The place where the built war-file will appear
SOURCE_ARTIFACTS_DIR=$S2I_SOURCE_DIR


echo "S2I_SOURCE_DIR: $S2I_SOURCE_DIR"
echo "S2I_ARTIFACTS_DIR: $S2I_ARTIFACTS_DIR"
echo "BUILD_NUMBER: $BUILD_NUMBER"
echo "COMMITID: $COMMITID"

mkdir -p $S2I_ARTIFACTS_DIR

# If the assemble script is executed with the '-h' flag, print the usage.
if [[ "$1" == "-h" ]]; then
    exec /usr/libexec/s2i/usage
fi

# Restore artifacts from the previous build (if they exist).
#
if [ "$(ls $S2I_ARTIFACTS_DIR 2>/dev/null)" ]; then
  echo "---> Restoring build artifacts..."
  mv $S2I_ARTIFACTS_DIR/. ./
fi

# Make one or more folders and all subfolders writeable
function make_writeable() {
    for d in $*; do
        [ -d "$d" ] && chmod -R a+rwx $d
    done
}

# Start build using gradle.
if [ -f "$S2I_SOURCE_DIR/build.gradle" ]; then
    echo "---> User $(id) home is $HOME"

    GRADLE_ARGS=${GRADLE_ARGS:-"--no-daemon build"}
    if [ -z "$SONARQUBE_TOKEN" ]; then GRADLE_ARGS=$GRADLE_ARGS; else GRADLE_ARGS="$GRADLE_ARGS  sonarqube -Dsonar.host.url=https://hsa-sonarqube.nordicmedtest.se -Dsonar.login=$SONARQUBE_TOKEN"; fi

    echo "---> Building application from source with gradle wrapper GRADLE_ARGS=$GRADLE_ARGS"
    (cd $S2I_SOURCE_DIR; ./gradlew $GRADLE_ARGS)
    ERR=$?
    if [ $ERR -ne 0 ]; then
        echo "Aborting due to error code $ERR from gradle build"
        exit $ERR
    fi

    WAR=$(find ${SOURCE_ARTIFACTS_DIR} -name \*.war)
    if [ -f "$WAR" ]; then
        echo "---> Move output artifact $WAR to $S2I_ARTIFACTS_DIR"
        mv $WAR $S2I_ARTIFACTS_DIR
        ERR=$?
        if [ $ERR -ne 0 ]; then
            echo "Unable to move output artifact, exit code: $ERR"
            exit $ERR
        fi
    else
        echo "---> Output WAR file $WAR could not be found in directory $SOURCE_ARTIFACTS_DIR"
        exit 1
    fi

    make_writeable $GRADLE_USER_HOME $S2I_SOURCE_DIR

else
    echo "---> No such gradle.build file in directory $S2I_SOURCE_DIR"
    exit 1
fi

