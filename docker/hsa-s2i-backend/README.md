
# Creating a basic S2I builder image  

## Getting started  

s2i CLI tool (click releases to download): https://github.com/openshift/source-to-image

To build the image:

### Files and Directories  
| File                   | Required? | Description                                                  |
|------------------------|-----------|--------------------------------------------------------------|
| Dockerfile             | Yes       | Defines the base builder image                               |
| s2i/bin/assemble       | Yes       | Script that builds the application                           |
| s2i/bin/usage          | No        | Script that prints the usage of the builder                  |
| s2i/bin/run            | Yes       | Script that runs the application                             |
| s2i/bin/save-artifacts | No        | Script for incremental builds that saves the built artifacts |
| test/run               | No        | Test script for the builder image                            |
| test/test-app          | Yes       | Test application source code                                 |

#### Dockerfile
Create a *Dockerfile* that installs all of the necessary tools and libraries that are needed to build and run our application.  This file will also handle copying the s2i scripts into the created image.

#### S2I scripts

##### assemble
Create an *assemble* script that will build our application, e.g.:
- build python modules
- bundle install ruby gems
- setup application specific configuration

The script can also specify a way to restore any saved artifacts from the previous image.   

##### run
Create a *run* script that will start the application. 

##### save-artifacts (optional)
Create a *save-artifacts* script which allows a new build to reuse content from a previous version of the application image.

##### usage (optional) 
Create a *usage* script that will print out instructions on how to use the image.

##### Make the scripts executable 
Make sure that all of the scripts are executable by running *chmod +x s2i/bin/**

#### Create the builder image
The following command will create a builder image named hsa-s2i-backend based on the Dockerfile that was created previously.
```
docker build -t hsa-s2i-backend .
```
The builder image can also be created by using the *make* command since a *Makefile* is included.

Once the image has finished building, the command *s2i usage hsa-s2i-backend* will print out the help info that was defined in the *usage* script.

#### Creating the artifact image
The artifact image combines the builder image with your applications source code, which is served using hsa-s2i-backend application is installed via the *Dockerfile*, compiled using the *assemble* script, and run using the *run* script.
The following command will create the artifact image:
```
s2i build -c . hsa-s2i-backend hsa-organization-artifact
---> Building and installing application from source...
```
Using the logic defined in the *assemble* script, s2i will now create an application image using the builder image as a base and including the source code from the test/test-app directory. 

#### Running tests on the artifact image
To run a test suite through a gradle task, we execute the "run"-script in the artifact image through a normal docker run, and give a comma separated list of the gradle tasks that should be run, the report directory (inside the container) as well as mount a local volume to that directory:
```
docker run -e TESTS=<comma_separated_list_of_tests> -e REPORT_DIR=/mnt/reports -v <local_report_dir>:/mnt/reports hsa-organization-artifact /usr/libexec/s2i/run
```

#### Using the saved artifacts script
Rebuilding the application using the saved artifacts can be accomplished using the following command:
```
s2i build --incremental=true test/test-app nginx-centos7 nginx-app
---> Restoring build artifacts...
---> Building and installing application from source...
```
This will run the *save-artifacts* script which includes the custom code to backup the currently running application source, rebuild the application image, and then re-deploy the previously saved source using the *assemble* script.
