# Builder for HSA Backend services
FROM openshift/base-centos7

LABEL maintainer="inera.se"

# Install Java and Chrome etc.
RUN yum -y update
RUN yum -y install java-11-openjdk-devel git

# Rename the builder environment variable to inform users about application you provide them
ENV BUILDER_VERSION 1.0
ENV GRADLE_USER_HOME /tmp/.gradle

LABEL io.k8s.description="Platform for building HSA backend services" \
      io.k8s.display-name="builder backend-services" \
      io.openshift.tags="builder,backend"

# Default project UID  
ARG X_UID=1001
ENV X_UID=${X_UID}

# Prepare the build environment by downloading dependencies to make the build go faster
RUN git clone https://bitbucket.org/ineraservices/hsa-organization.git --depth=1 && \
    git clone https://bitbucket.org/ineraservices/hsa-ldap-proxy.git --depth=1 && \
    cd hsa-organization && ./gradlew --no-daemon resolveDependencies && cd .. && rm -rf hsa-organization && \
    cd hsa-ldap-proxy && ./gradlew --no-daemon resolveDependencies && cd .. && rm -rf hsa-ldap-proxy

# Env (localtime)
COPY environment /etc/environment
RUN ln -snf /usr/share/zoneinfo/Europe/Stockholm /etc/localtime
RUN localedef -i en_US -f UTF-8 en_US.UTF-8
ENV LANG=en_US.UTF-8

# S2I runs as user default, but when invoked by the testrunner pod another user (uid) is active (depends on openshift project settings)
# Setup permissions to enable an unknown user to access different locations
ARG X_DIRS="/tmp/artifacts /tmp/src /tmp/nohome /tmp/reports /tmp/.gradle /tmp/sonar/cache /tmp/sonar/_tmp"
RUN mkdir -p ${X_DIRS} && chown -R ${X_UID} ${X_DIRS} && chmod -R 777 ${X_DIRS}

# Copy the S2I scripts to /usr/libexec/s2i
ADD s2i/bin/ /usr/libexec/s2i

# This default user is created in the openshift/base-centos7 image
USER ${X_UID}

CMD ["/usr/libexec/s2i/usage"]
