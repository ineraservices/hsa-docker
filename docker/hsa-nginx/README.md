### HSA nginx

This image contains an nginx server that runs as an unprivileged user. It listens to 8080 and is used to supply the frontend application, as well as (if configuration is uncommented) proxy requests to our APIs from the frontend application.

default.conf contains the configuration.
